<?php

/**
 * @file
 * Provides a Bootstrap dropdown button to switch between available languages.
 */

/**
 * Implements hook_libraries_info().
 */
function bootstrap_languages_libraries_info() {
  $libraries['bootstrap-languages'] = array(
    'name' => 'Languages for Bootstrap 3',
    'vendor url' => 'http://usrz.github.io/bootstrap-languages/',
    'download url' => 'https://github.com/usrz/bootstrap-languages/archive/master.zip',
    'version' => '2.0',
    'files' => array(
      'css' => array('languages.min.css'),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_block_info().
 */
function bootstrap_languages_block_info() {
  include_once DRUPAL_ROOT . '/includes/language.inc';
  $block = array();
  $info = language_types_info();
  foreach (language_types_configurable(FALSE) as $type) {
    $block[$type] = array(
      'info' => t('Bootstrap Languages (@type)', array('@type' => $info[$type]['name'])),
      'cache' => DRUPAL_NO_CACHE,
    );
  }
  return $block;
}


/**
 * Implements hook_block_view().
 */
function bootstrap_languages_block_view($type = 'language') {
  if (drupal_multilingual()) {

    $options = array();
    $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
    $languages = language_negotiation_get_switch_links($type, $path);

    // Load the Languages for Bootstrap 3 library.
    $library = libraries_load('bootstrap-languages');
    if (!$library['loaded']) {
      watchdog('Bootstrap Languages', $library['error message'], NULL, WATCHDOG_ERROR);
      return;
    }


    // Now we iterate on $languages to build needed options for dropdown.
    foreach ($languages->links as $lang_code => $lang_options) {

      // Build the options in an associative array (lang_code => lang_link)
      $href = isset($lang_options['href']) ? $lang_options['href'] : '<front>';
      $lang_link = check_plain(url($href, array(
        'language' => $lang_options['language'],
        'query' => isset($lang_options['query']) ? $lang_options['query'] : '',
      )));
      $options[$lang_code] = $lang_link;

    }

    // Position language switcher to the right of the navbar.
    drupal_add_css($library['library path'] . '/languages.min.css');
    drupal_add_css('.navbar #block-bootstrap-languages-language
    { float: right; margin: 8px 0px 8px 5px; }
    .btn.btn-sm > .lang-sm { top: 2px !important; }', array('type' => 'inline'));

    // Build markup for the Bootstrap languages switcher.
    global $language;
    $markup = '<div class="btn-group">';
    $markup .= '  <button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">';
    $markup .= '    <span class="lang-xs" lang="' . $language->language . '"></span> <span class="caret"></span>';
    $markup .= '  </button>';
    $markup .= '  <ul class="dropdown-menu" role="menu">';
    foreach ($options as $lang_code => $lang_link) {
      if($lang_link != current_path()){
        $markup .= '    <li><a href="' . $lang_link . '"><span class="lang-xs lang-lbl" lang="' . $lang_code . '"></span></a></li>';
      }

    }
    $markup .= '  </ul>';
    $markup .= '</div>';

    // Set the block subject and content
    if (isset($languages->links)) {
      $block['subject'] = t('Languages');
      $block['content'] = $markup;
      return $block;
    }
  }
}

